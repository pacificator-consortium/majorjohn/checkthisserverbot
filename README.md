# Code source du bot CheckThisServer (NoName)

Requière: Kotlin

Tire en dépendance la librairie https://gitlab.com/pacificator-consortium/majorjohn/checkthisserver qui n'est pas encore release. La branche Integrate-With-Bot est utilisé. 


# Les fichiers de config :
Le token.cfg contien uniquement le token en brut du bot trouvable sur https://discordapp.com/developers/applications/me

# Ajouter un bot :
Pour ajouter un bot à un salon il faut le client_id du bot ainsi que générer un chiffre correspondant au permission voulu. Des outils sont disponible coté outils developper de discord
https://discordapp.com/oauth2/authorize?client_id=[CLIENT_ID]&scope=bot&permissions=[PERMISSIONS]

## Reset les commandes du bot :

Il faut expulser le bot et le re-inviter dans le lobby

# Installation avec Docker

Le projet doit déjà avoir été build en local avec gradle avec la commande  "gradle clean jar". Cela met à disposition le jar dans le dossier build/libs.
Un dossier dist doit être contenu à la racine. Il contient le mount bind de dossier de resources et config pour docker.
Le dossier dist doit contenir un sous dossier config avec le token du bot.

Pour la compilation :
```
docker-compose build
```

Pour l'execution :
```
docker-compose up
```

Pour l'execution sur le server :
```
docker-compose up --detach
```

Pour stop l'excution sur le server :
```
docker-compose down
```



# To Do List :

# [X.X.X]
- [ ] Commande de scan configurable (Nb de thread etc ...)
- [ ] Gestion cohérente des multiscan (Multiple écriture par des scans différent dans les même fichier)
- [ ] Ajout d'un filewatcher sur le fichier d'ip minecraft afin de notifier les utilisateur du discord qu'une nouvelle adresse a été trouvé
- [ ] Commande pour clear la liste des scans
- [ ] Système de stats d'utilisation de commande
- [ ] Système de scan toute les X seconds pour savoir si un serveur minecraft est up ou down avec notification sur un chat
- [ ] Implementer la commande minecraftserverlist sans paramêtre (Montre les dernier serveur trouvé)
- [ ] Une commande pour effectuer des recherches sur les server minecraft (filtre par version)


# [1.1.2]
- [ ] Trier le resultat de serverminecraftlist rescan par version
- [ ] Commande donnant la version du bot (Et de la lib ?)
- [ ] Ajouter l'heure de scan au serveur minecraft format txt

# [1.1.1] [Released]
- [X] Une commande pour re-scanner des serveurs
- [X] Une commande pour lire le resultat des rescan
- [X] [0.1.1] La rotation a un nombre de scan trop bas
- [X] Utilisation d'un vrai logger

# [1.1.0] [jdk-21] [Released]
- [X] Utilisation des threads virtuelle
- [X] NReg sur le log (Est-ce que toute les ip sont bien enregistré après le changement de fonctionnement de la queue)
- [ ] ~~Système pour conserver suffisament de puissance pour l'instant discord du bot.~~ [voir bug parallelism](https://gitlab.com/pacificator-consortium/majorjohn/checkthisserver)
- [X] [1.0.3-Dev] La dockerisation, bottleneck le scan à 10 scan à la seconde (pour windows, 20 pour linux)

# [1.0.2] [Released]
- [X] récupère la dernière ip scanné
- [X] /addvisitedserver : Permet d'ajouter des info sur les serveur
- [X] /visitedserverlist : Permet de voir les différents serveur visité
- [X] [1.0.1] La commande MinecraftServerList ne répond pas si l'index de page est trop elevé.

# [1.0.1] [Released]
- [X] [0.2.0-Dev] En cas de status ROTATING, le scan ne peut être stoppé : Il faut afficher un message d'erreur dans ce cas
- [X] [0.2.0-Dev] L'unité de temps dans la commande /scans est manquant
- [X] [0.2.0-Dev] La commande /scans affiche des ms au lieu de s
- [X] [0.2.0-Dev] Si aucun scan n'a été lancé la commande /scans plante
- [X] Suppression des fichiers de log d'ip inutile (Mise en param du scan ? (Debug vs Prod))
- [X] Purger le champs addressScanned lors d'un stop manuel
- [X] [1.0.0] Les commandes sont limités à 2k de caractère. La commande MinecraftServerList peut en afficher une infinité -> Découper la commande

# [1.0.0] [Released]
- [X] Customiser l'image du bot
- [X] Release de la lib propre + bot
- [X] Gestion du guildId de manière dynamique : lorsque le bot est ajouté a un serveur celui-ci doit dirrectement être opérationnel.
- [X] ~~Ajout d'un systeme de permission. (Les utilisateur standart ne peuvent pas lancer de scan)~~ -> Natif avec le système de commande discord

# [0.3.0]
- [X] Correction des bug majeurs


# [0.2.0]
- [X] Commande de scan configurable (Avec un argument : On spécifie l'ip de départ en ordre naturel)
- [X] Voir la liste des scans en cours
- [X] Pouvoir stopper un scan manuellement
- [X] Une commande de scan de 0.0.0.0 à 255.255.255.255
- [X] Rework la commande resume pour compacter les données (Voir les stats au global)


# [0.1.0]
- [X] Set up lancement d'une commande
- [X] Cmd: Lancement d'un scan
- [X] Cmd: Récupération des informations scannée
- [X] Dockerisation
- [X] Deploiement


# [Bug]

- Mineur :
    - [X] [0.2.0-Dev] En cas de status ROTATING, le scan ne peut être stoppé : Il faut afficher un message d'erreur dans ce cas
    - [X] [0.2.0-Dev] L'unité de temps dans la commande /scans est manquant
    - [X] [0.2.0-Dev] La commande /scans affiche des ms au lieu de s
    - [X] [0.2.0-Dev] Si aucun scan n'a été lancé la commande /scans plante
    - [X] [0.2.0-Dev] Après plusieur heure de scan, il semble y avoir des ralentisement -> Du à la queue qui est en sous capacité car le thread qui l'alimente n'a pas la priorité. Le contournement est de réduire le nombre de thread de l'executor (Le bug passe en mineur)
    - [X] [1.0.1] La commande MinecraftServerList ne répond pas si l'index de page est trop elevé.
    - [ ] [1.1.1] Le mode debug est actif sur les scans
- Majeur :
   - [X] [0.2.0-Dev] Format d'ipv4 invalide : 93.188.161.166
   - [X] [0.1.1] La rotation a un nombre de scan trop bas
   - [ ] [1.1.1] Les multi scan semble buggué. Le deuxième scan reste à 0.
- Bloquant :
   - [X] [1.0.0] Le système de ping avec le protocol Minecraft ne fonctionne pas (Exception en permanence)
   - [X] [1.0.0] Les commandes sont limités à 2k de caractère. La commande MinecraftServerList peut en afficher une infinité -> Découper la commande
   - [X] [1.1.0-Dev] La dockerisation, bottleneck le scan à 10 scan à la seconde (pour windows, 20 pour linux) (Cf bug parallelism)