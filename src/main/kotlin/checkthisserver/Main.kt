package checkthisserver

import checkthisserver.api.Command
import checkthisserver.internal.api_impl.cmd.CommandAddVisitedMcServer
import checkthisserver.internal.api_impl.cmd.CommandIsAlive
import checkthisserver.internal.api_impl.cmd.CommandMinecraftServerList
import checkthisserver.internal.api_impl.cmd.CommandResume
import checkthisserver.internal.api_impl.cmd.CommandRunScan
import checkthisserver.internal.api_impl.cmd.CommandScans
import checkthisserver.internal.api_impl.cmd.CommandStopScan
import checkthisserver.internal.api_impl.cmd.CommandVisitedServerList
import checkthisserver.service.ChunkedStringService
import checkthisserver.service.ReScanMcServerService
import checkthisserver.service.VisitedMcServerService
import dev.kord.core.Kord
import dev.kord.gateway.Intent
import dev.kord.gateway.PrivilegedIntent
import org.apache.logging.log4j.LogManager
import java.io.File

private const val TOKEN_PATH = "config/token.cfg"

private val LOGGER = LogManager.getLogger("Main.kt")
suspend fun main() {

    LOGGER.info("Starting bot : NoName")

    val visitedMcServerService = VisitedMcServerService()
    val reScanMcServerService = ReScanMcServerService()
    val chunkedStringService = ChunkedStringService()

    LOGGER.info(
        "Thread number used : " + System.getProperty(
            "jdk.virtualThreadScheduler.parallelism",
            Runtime.getRuntime().availableProcessors().toString()
        )
    )

    val commands: List<Command> =
        listOf(
            CommandIsAlive(),
            CommandRunScan(),
            CommandResume(),
            CommandMinecraftServerList(chunkedStringService, reScanMcServerService),
            CommandScans(),
            CommandStopScan(),
            CommandAddVisitedMcServer(visitedMcServerService),
            CommandVisitedServerList(visitedMcServerService, chunkedStringService)
        )

    LOGGER.info("Start discord connection")
    try {
        val kord = Kord(File(TOKEN_PATH).readText(Charsets.UTF_8))
        LOGGER.info("Finish discord connection")

        commands.forEach { cmd -> cmd.register(kord) }

        kord.login {
            @OptIn(PrivilegedIntent::class)
            intents += Intent.MessageContent
        }
    } catch (e: Exception) {
        LOGGER.error("Failed discord connection")
        e.printStackTrace()
    }
}

