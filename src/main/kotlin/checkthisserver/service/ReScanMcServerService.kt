package checkthisserver.service

import checkthisserver.pojo.MinecraftServer
import fr.draftcorporation.services.LogService
import kotlinx.serialization.json.Json
import kotlin.io.path.Path


val RESCAN_MC_SERVER_PATH = Path(LogService.WORKING_DIR_NAME).resolve(LogService.FILENAME_RESCAN_JSON)!!

class ReScanMcServerService {


    fun toStringList(): List<String> {
        return this.load()
            .map { server ->
                "${server.ip} ${server.version}"
            }.toList()
    }

    private fun load(): List<MinecraftServer> {
        val file = RESCAN_MC_SERVER_PATH.toFile()
        return if (file.exists()) {
            val jsonStringify = file.readText()
            Json.decodeFromString<List<MinecraftServer>>(jsonStringify).toMutableList()
        } else {
            listOf()
        }
    }
}