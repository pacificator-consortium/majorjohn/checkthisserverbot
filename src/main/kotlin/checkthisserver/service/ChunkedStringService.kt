package checkthisserver.service

private const val MAX_DISCORD_CHAR_BY_MESSAGE = 2000

class ChunkedStringService {

    fun getPage(list: List<String>, pageNumber: Long): String {
        var currentSize = 0
        var messageSize = 0
        // Add one line because of unzip + first (the last line will be always remove otherwise)
        var result = (list + sequenceOf(""))
            .distinct()
            .zipWithNext()
            .dropWhile { x ->
                if (currentSize == 0) {
                    // +1 for the \n
                    currentSize = x.first.length + 1
                }
                // +1 for the \n
                currentSize += x.second.length + 1
                (pageNumber) * MAX_DISCORD_CHAR_BY_MESSAGE >= currentSize
            }
            .takeWhile { x ->
                // +1 for the \n
                if (messageSize == 0) {
                    messageSize = x.first.length + 1
                }
                // +1 for the \n
                messageSize += x.second.length + 1
                messageSize <= MAX_DISCORD_CHAR_BY_MESSAGE
            }
            .unzip()
            .first
            .joinToString("\n")
        if(result.isEmpty()) {
            result = "No result for the page: $pageNumber"
        }
        return result
    }

}