package checkthisserver.service

import checkthisserver.pojo.VisitedMcServer
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File


private const val WORKING_DIRECTORY = "currentResult"
private const val FILE_NAME = "server_document.json"
private const val VISITED_MC_SERVER_PATH = "$WORKING_DIRECTORY/$FILE_NAME"

class VisitedMcServerService {

    private var mutableMcServerListInformation: MutableList<VisitedMcServer> = mutableListOf()

    fun add(newVisitedMcServer: VisitedMcServer) {
        if (mutableMcServerListInformation.isEmpty()) {
            this.load()
        }
        mutableMcServerListInformation.removeIf { server -> server.address == newVisitedMcServer.address }
        mutableMcServerListInformation.add(newVisitedMcServer)
        this.save()
    }

    fun toStringList(): List<String> {
        if (mutableMcServerListInformation.isEmpty()) {
            this.load()
        }
        return mutableMcServerListInformation.stream()
            .map { server ->
                var extra = ""
                if (server.isWhiteList || server.isModedClient) {
                    extra = "("
                    if (server.isWhiteList) {
                        extra += "WL"
                    }
                    if (server.isModedClient) {
                        if (extra.length > 1) {
                            extra += ","
                        }
                        extra += "MOD"
                    }
                    extra += ")"
                }
                "${server.address} ${server.version} ${server.explorationDate} $extra"
            }.toList()
    }

    private fun save() {
        val jsonStringify = Json.encodeToString(mutableMcServerListInformation)
        File(VISITED_MC_SERVER_PATH).writeText(jsonStringify)
    }

    private fun load() {
        val file = File(VISITED_MC_SERVER_PATH)
        if (file.exists()) {
            val jsonStringify = file.readText()
            mutableMcServerListInformation = Json.decodeFromString<List<VisitedMcServer>>(jsonStringify).toMutableList()
        }
    }
}