package checkthisserver.api

import dev.kord.core.Kord
import dev.kord.core.entity.interaction.GuildChatInputCommandInteraction

interface Command {
    suspend fun register(kord: Kord)

    fun getCommandName(): String

    fun getCommandDescription(): String

    fun executeCommand(interaction: GuildChatInputCommandInteraction): String
}
