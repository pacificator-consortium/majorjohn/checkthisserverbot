package checkthisserver.pojo

import kotlinx.serialization.Serializable

@Serializable
class MinecraftServer(
    val ip: String,
    val port: String,
    val version: String
)