package checkthisserver.pojo

import kotlinx.serialization.Serializable


@Serializable
data class VisitedMcServer(
    val address: String,
    val version: String,
    val explorationDate: String,
    val comment: String,
    val isWhiteList: Boolean,
    val isModedClient: Boolean
)