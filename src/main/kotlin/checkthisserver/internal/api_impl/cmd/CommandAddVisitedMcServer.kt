package checkthisserver.internal.api_impl.cmd

import checkthisserver.pojo.VisitedMcServer
import checkthisserver.service.VisitedMcServerService
import dev.kord.core.entity.interaction.GuildChatInputCommandInteraction
import dev.kord.rest.builder.interaction.ChatInputCreateBuilder
import dev.kord.rest.builder.interaction.boolean
import dev.kord.rest.builder.interaction.string

class CommandAddVisitedMcServer(private val visitedMcServerService: VisitedMcServerService) : AbstractCommand() {

    override fun getCommandName(): String {
        return "addvistedserver"
    }

    override fun getCommandDescription(): String {
        return "Add information on a server"
    }

    override fun commandParameters(): ChatInputCreateBuilder.() -> Unit {
        return {
            string("ipv4", "server ip") {
                required = true
            }
            string("version", "server version") {
                required = true
            }
            string("date", "exploration date") {
                required = true
            }
            string("comment", "Some extra comment") {
            }
            boolean("whitelist", "is there a whitelist ? (True by default)") {
            }
            boolean("modedclient", "is there a specific client needed ? (False by default)") {
            }
        }
    }

    override fun executeCommand(interaction: GuildChatInputCommandInteraction): String {
        val command = interaction.command
        val ipv4 = command.strings["ipv4"]!!
        val version = command.strings["version"]!!
        val date = command.strings["date"]!!

        val comment = command.strings["comment"] ?: ""
        val isWhiteList = command.booleans["whitelist"] ?: true
        val isModedClient = command.booleans["modedclient"] ?: false

        visitedMcServerService.add(VisitedMcServer(ipv4, version, date, comment, isWhiteList, isModedClient))

        return "Successfully add !"
    }
}