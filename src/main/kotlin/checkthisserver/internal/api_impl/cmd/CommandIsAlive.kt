package checkthisserver.internal.api_impl.cmd

import dev.kord.core.entity.interaction.GuildChatInputCommandInteraction

class CommandIsAlive() : AbstractCommand() {
    override fun getCommandName(): String {
        return "isalive"
    }

    override fun getCommandDescription(): String {
        return "Check if the bot is still alive"
    }

    override fun executeCommand(interaction: GuildChatInputCommandInteraction): String {
        return "Yes, i'm alive"
    }
}