package checkthisserver.internal.api_impl.cmd

import dev.kord.core.entity.interaction.GuildChatInputCommandInteraction
import dev.kord.rest.builder.interaction.ChatInputCreateBuilder
import dev.kord.rest.builder.interaction.int
import dev.kord.rest.builder.interaction.integer
import fr.draftcorporation.pojo.ScanStatus
import fr.draftcorporation.services.ScanRepository

class CommandStopScan() : AbstractCommand() {
    override fun getCommandName(): String {
        return "stopscan"
    }

    override fun getCommandDescription(): String {
        return "stop selected scan"
    }

    override fun commandParameters(): ChatInputCreateBuilder.() -> Unit {
        return {
            integer("index", "The scan index to stop") {
                required = true
            }
        }
    }

    override fun executeCommand(interaction: GuildChatInputCommandInteraction): String {
        val command = interaction.command
        val scanIndex = command.integers["index"]!!

        return if (scanIndex >= ScanRepository.getInstance().scanList.size) {
            "Unreachable scan. Fail to stop"
        } else {
            return when (ScanRepository.getInstance().scanList[scanIndex.toInt()].scanStatus) {
                ScanStatus.ROTATING -> "Scan can't be stopped during rotating"
                ScanStatus.STOP -> "Scan already stop"
                else -> { // Note the block
                    ScanRepository.getInstance().scanList[scanIndex.toInt()].stop()
                    return "Scan stop successful"
                }
            }
        }
    }
}