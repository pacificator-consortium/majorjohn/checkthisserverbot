package checkthisserver.internal.api_impl.cmd

import checkthisserver.service.ChunkedStringService
import checkthisserver.service.RESCAN_MC_SERVER_PATH
import checkthisserver.service.ReScanMcServerService
import dev.kord.core.entity.interaction.GuildChatInputCommandInteraction
import dev.kord.rest.builder.interaction.ChatInputCreateBuilder
import dev.kord.rest.builder.interaction.boolean
import dev.kord.rest.builder.interaction.integer
import fr.draftcorporation.services.LogService
import java.io.File
import kotlin.io.path.Path

class CommandMinecraftServerList(
    private val chunkedStringService: ChunkedStringService,
    private val reScanMcServerService: ReScanMcServerService
) : AbstractCommand() {
    override fun getCommandName(): String {
        return "minecraftserverlist"
    }

    override fun getCommandDescription(): String {
        return "show all minecraft server found during scans (Show the latest by default)"
    }

    override fun commandParameters(): ChatInputCreateBuilder.() -> Unit {
        return {
            integer("pagenumber", "the page number") {
            }
            boolean("showrawdata", "Show raw data from working scan file (default= true)") {
            }
        }
    }

    override fun executeCommand(interaction: GuildChatInputCommandInteraction): String {
        val command = interaction.command
        val pageNumber = command.integers["pagenumber"]
        val showRawData = command.booleans["showrawdata"] ?: true

        val minecraftServerFile: File = if (showRawData) {
            Path(LogService.WORKING_DIR_NAME).resolve(LogService.FILENAME_MINECRAFT_SERVER).toFile()
        } else {
            RESCAN_MC_SERVER_PATH.toFile()
        }

        val minecraftServerList: String =
            if (!minecraftServerFile.exists() || minecraftServerFile.length().toInt() == 0) {
                "No server found for now"
            } else {
                if (pageNumber == null) {
                    "latest not implemented"
                } else {
                    if (showRawData) {
                        File(LogService.WORKING_DIR_NAME + "/" + LogService.FILENAME_MINECRAFT_SERVER)
                            .useLines { lines: Sequence<String> ->
                                chunkedStringService.getPage(lines.toList(), pageNumber)
                            }
                    } else {
                        chunkedStringService.getPage(reScanMcServerService.toStringList(), pageNumber)
                    }
                }
            }

        return minecraftServerList
    }
}