package checkthisserver.internal.api_impl.cmd

import dev.kord.core.entity.interaction.GuildChatInputCommandInteraction
import fr.draftcorporation.services.LogService
import kotlin.io.path.Path
import kotlin.io.path.readText

class CommandResume() : AbstractCommand() {
    override fun getCommandName(): String {
        return "resume"
    }

    override fun getCommandDescription(): String {
        return "show scan resume result"
    }

    override fun executeCommand(interaction: GuildChatInputCommandInteraction): String {
        return Path(LogService.WORKING_DIR_NAME).resolve(LogService.FILENAME_RESUME_JSON).readText()
    }
}