package checkthisserver.internal.api_impl.cmd

import dev.kord.core.entity.interaction.GuildChatInputCommandInteraction
import fr.draftcorporation.services.ScanRepository

class CommandScans() : AbstractCommand() {
    override fun getCommandName(): String {
        return "scans"
    }

    override fun getCommandDescription(): String {
        return "show all scan status"
    }

    override fun executeCommand(interaction: GuildChatInputCommandInteraction): String {
        var result = ""
        if (ScanRepository.getInstance().scanList.isEmpty()) {
            result = "No scan launched"
        } else {
            ScanRepository.getInstance().scanList.forEachIndexed { index, element ->
                result += "scan $index " +
                        "status : ${element.scanStatus} " +
                        "duration: ${element.executionTime / 1000}s " +
                        "current rotation server scanned : ${element.addressScanned.size} " +
                        "nb of thread ${element.scanServiceParameter.threadNumber()} " +
                        "last ip indicator ${element.lastServerScanIndicator} " +
                        "\n"
            }
        }
        return result
    }
}