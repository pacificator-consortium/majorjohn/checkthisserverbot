package checkthisserver.internal.api_impl.cmd

import checkthisserver.service.ChunkedStringService
import checkthisserver.service.VisitedMcServerService
import dev.kord.core.entity.interaction.GuildChatInputCommandInteraction
import dev.kord.rest.builder.interaction.ChatInputCreateBuilder
import dev.kord.rest.builder.interaction.int
import dev.kord.rest.builder.interaction.integer

class CommandVisitedServerList(
    private val visitedMcServerService: VisitedMcServerService,
    private val chunkedStringService: ChunkedStringService
) : AbstractCommand() {
    override fun getCommandName(): String {
        return "visitedserverlist"
    }

    override fun getCommandDescription(): String {
        return "show all visited minecraft server"
    }

    override fun commandParameters(): ChatInputCreateBuilder.() -> Unit {
        return {
            integer("pagenumber", "the page number") {
                required = true
            }
        }
    }

    override fun executeCommand(interaction: GuildChatInputCommandInteraction): String {
        val command = interaction.command
        val pageNumber = command.integers["pagenumber"]!!

        return chunkedStringService.getPage(visitedMcServerService.toStringList(), pageNumber)
    }
}