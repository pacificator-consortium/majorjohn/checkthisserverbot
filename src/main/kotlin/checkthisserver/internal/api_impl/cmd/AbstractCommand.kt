package checkthisserver.internal.api_impl.cmd

import checkthisserver.api.Command
import dev.kord.core.Kord
import dev.kord.core.behavior.interaction.response.respond
import dev.kord.core.event.interaction.GuildChatInputCommandInteractionCreateEvent
import dev.kord.core.on
import dev.kord.rest.builder.interaction.ChatInputCreateBuilder

abstract class AbstractCommand : Command {


    override suspend fun register(kord: Kord) {
        val builder: ChatInputCreateBuilder.() -> Unit = commandParameters()

        kord.createGlobalChatInputCommand(
            this.getCommandName(),
            this.getCommandDescription(),
            builder
        )

        // Ughhhh ... disgusting
        val that = this

        kord.on<GuildChatInputCommandInteractionCreateEvent> {
            val command = interaction.command

            if (command.rootName == that.getCommandName()) {
                val response = interaction.deferPublicResponse()

                response.respond { content = that.executeCommand(interaction) }
            }
        }
    }

    protected open fun commandParameters(): ChatInputCreateBuilder.() -> Unit {
        return { }
    }
}