package checkthisserver.internal.api_impl.cmd

import dev.kord.core.entity.interaction.GuildChatInputCommandInteraction
import dev.kord.rest.builder.interaction.ChatInputCreateBuilder
import dev.kord.rest.builder.interaction.boolean
import dev.kord.rest.builder.interaction.integer
import dev.kord.rest.builder.interaction.string
import fr.draftcorporation.pojo.ScanServiceParameter
import fr.draftcorporation.pojo.ScanningType
import fr.draftcorporation.services.ScanService
import kotlin.concurrent.thread

class CommandRunScan() : AbstractCommand() {

    override fun getCommandName(): String {
        return "runscan"
    }

    override fun getCommandDescription(): String {
        return "Launch a scan from ipv4 (0.0.0.0 by default) and increment it"
    }

    override fun commandParameters(): ChatInputCreateBuilder.() -> Unit {
        return {
            integer("threadnumber", "number of thread to scan server") {
                required = true
            }
            string("ipv4", "start ip to scan") {
            }
            boolean("rescan", "If true, will rescan all minecraft server (default to false)") {
            }
        }
    }

    override fun executeCommand(interaction: GuildChatInputCommandInteraction): String {
        val command = interaction.command
        val threadNumber = command.integers["threadnumber"]!!
        val ipv4 = command.strings["ipv4"]
        val rescan = command.booleans["rescan"]?: false

        val scanServiceParameterBuilder = ScanServiceParameter.ScanServiceParameterBuilder()
            .setThreadNumber(threadNumber.toInt())
            .setProcessTimeSecond(-1)
            .setRotatingServerNumber(300000)
            .setQueueSizeFactor(20)
            .setScanningType(if (rescan) ScanningType.RESCAN else ScanningType.ZERO_TO_ALL)
            .setDebug(true)
        if (ipv4 != null) {
            val regex =
                Regex("^((25[0-5]|(2[0-4]|1\\d|[1-9]|)\\d)\\.?\\b){4}\$")

            if (regex.matches(ipv4)) {
                scanServiceParameterBuilder.setIpv4(ipv4)
            } else {
                return "wrong ipv4 format"
            }
        }

        thread {
            ScanService().runScanService(
                scanServiceParameterBuilder
                    .build()
            )
        }

        return "Scan launched !"
    }
}