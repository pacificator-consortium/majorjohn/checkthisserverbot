FROM eclipse-temurin:21

WORKDIR /app

COPY build/libs/checkthisserverbot-*.jar ./checkthisserverbot.jar

CMD ["java", "-Djdk.virtualThreadScheduler.parallelism=30", "-jar", "checkthisserverbot.jar"]